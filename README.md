# Router Where Art Thou?

This chal gives you one of three router/firewall login pages (in various shades of janky) and all you have to do is login with the default creds for that brand.

* Desc: Interesting login page, think you can crack it?
* Flag: `TUCTF{y0u_f0und_th3_fun_r0ut3r_d3f4ult5}`
* Hint: Doesn't this page look...familiar?

## How To:
So it depends which login page you get, which is RNG. Login with the correct creds to get the flag.
* `0.html` (Fortinet) 
    * You can figure out that's the brand b/c of the name of the png in the source code of the page
    * Default creds for Fortinet are `admin`/`""`
* `1.html` (Sonicwall)
    * Sonicwall image and page name
    * Default creds are `admin`/`password`
* `2.html` (Palo Alto)
    * Big ass Palo logo
    * Default creds are `admin`/`admin`
