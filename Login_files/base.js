/*
 * Copyright(c) 2015, ${owner}
 * Created by hwong on 2/19/2015.
 */
umd(function (define) {
    define(function (require) {
        var Ext = require('Ext');
        var Pan = require('Pan');
        var util = require('pan/base/util');
        var msg = require('pan/base/msg');

        // reference local blank image
        Ext.BLANK_IMAGE_URL = '/images/s.gif';

        var isTxtNode = function ($node) {
            return $node.nodeType === 3 || $node.nodeType === 4;
        };

        // we need it in each of the top level esp files so that it can be called from the
        // save.esp file after a commit is done. Johnny said to put it here instead of
        // in each of the top level index.php files. If we put it here then it is accessible everywhere.
        window.checkPendingConfigChanges = function () {
            Pan.global.ContextVariables.retrieveConfigChangePending();
        };

        var base = {
            NETWORK_XPATH: "/config/devices/entry[@name='localhost.localdomain']/network/",
            VSYS_XPATH: "/config/devices/entry[@name='localhost.localdomain']/vsys/",
            SPAN_SLOT: "___SPAN___",
            msg: msg,
            isLoginPage: function (response) {
                return (response.responseText.indexOf("__LOGIN_PAGE__") >= 0);
            },
            checkUnauthorized: function (message) {
                if (message && message.startsWith('unauthorized')) {
                    var isCmsSelected = Pan.global.isCmsSelected();
                    var isCmsRemoteSession = Pan.global.isCmsRemoteSession();
                    var redirect = false;
                    if (message === 'unauthorized' && !isCmsSelected && !isCmsRemoteSession) {
                        redirect = true;
                    } else {
                        var result = message.split(":");
                        if (result && result.length > 1) {
                            if (isCmsRemoteSession) {
                                redirect = result[1] === "isCmsRemoteSession";
                            } else if (isCmsSelected) {
                                redirect = result[1] === "isCmsSelected";
                            }
                        }
                    }

                    if (redirect) {
                        Pan.base.redirectToLogout();
                    }
                }
            },
            redirectToLogout: function () {
                if (!Pan.inProcessRedirecting) {
                    Pan.inProcessRedirecting = true;
                    window.top.location = "/php/logout.php";
                    //Close socket
                    Pan.appframework.PanNotificationHandler.getInstance().close();
                }
                return false;
            },
            redirectIfExpired: function (response) {
                if (Pan.base.isLoginPage(response)) {
                    Pan.base.redirectToLogout();
                    return false; // no need to continue
                }
                return true;
            },

            showSystemAlarmViewer: function () {
                var win = Pan.AlarmMgr.getAlarmViewer();
                win.show(true);
            },

            isEmpty: util.isEmpty,
            /**
             * Return the number of entries
             */
            count: function (obj) {
                if (Ext.isObject(obj)) {
                    var count = 0;
                    for (var prop in obj) {
                        if (obj.hasOwnProperty(prop)) {
                            count++;
                        }
                    }
                    return count;
                } else if (Ext.isArray(obj)) {
                    return obj.length;
                } else {
                    return 0;
                }
            },
            defaultXpathId: function () {
                if (Pan.global.getLoc().val == '') {
                    return 'shared';
                } else {
                    return 'vsys';
                }
            },
            log: function () {
                if (window.console && window.console.log && Ext.isFunction(window.console.log)) {
                    window.console.log.apply(window.console, arguments);
                }
            },
            debug: function () {
                if (window.console && window.console.debug && Ext.isFunction(window.console.debug)) {
                    window.console.debug.apply(window.console, arguments);
                }
            },
            info: function () {
                if (window.console && window.console.info && Ext.isFunction(window.console.info)) {
                    window.console.info.apply(window.console, arguments);
                }
            },
            warn: function () {
                if (window.console && window.console.warn && Ext.isFunction(window.console.warn)) {
                    window.console.warn.apply(window.console, arguments);
                }
            },
            error: function () {
                if (window.console && window.console.error && Ext.isFunction(window.console.error)) {
                    // When the arguments[0] is an Error, console.error does not recognize it as such and treat it as
                    // regular object and print out "Error{}" which is not really helpful.
                    // I call toString() to get the message out of the error.
                    if (arguments.length > 0 && arguments[0] instanceof Error) {
                        window.console.error.call(window.console, arguments[0]);
                    }
                    else {
                        window.console.error.apply(window.console, arguments);
                    }
                }
            },
            dir: function () {
                if (window.console && window.console.dir && Ext.isFunction(window.console.dir)) {
                    window.console.dir.apply(window.console, arguments);
                }
            },
            trace: function () {
                if (window.console && window.console.trace && Ext.isFunction(window.console.trace)) {
                    window.console.trace.apply(window.console, arguments);
                }
            },
            getText: function (node) {
                return Ext.isIE ? node.text : node.textContent;
            },
            dateTimeRenderer: function (val) {
                if (val == undefined || val == "")
                    return "";
                var date = new Date(val * 1000);
                return date.format("Y/m/d H:i:s");
            },
            listRenderer: function (val) {
                if (val == undefined || val == "")
                    return "";
                var ul = [];
                for (var i = 0; i < val.length; i++) {
                    ul[i] = '<li>' + val[i] + '</li>';
                }
                var cb = ['<ul>', ul.join(''), '</ul>'];
                return cb.join('');
            },
            enableRenderer: function (val) {
                var enabled = val;
                if (val == undefined || val == "")
                    enabled = "no";

                var checkedImg = '/images/enable.gif';
                var uncheckedImg = '/images/disable.gif';
                return ''
                + '<div style="text-align:center;height:13px;overflow:visible">'
                + '<img style="vertical-align:-3px" src="'
                + (enabled == 'yes' ? checkedImg : uncheckedImg)
                + '"'
                + ' />'
                + '</div>'
                    ;
            },
            enableOrEmptyRenderer: function (val) {
                if (val == undefined)
                    return "";

                var checkedImg = '/images/enable.gif';
                var uncheckedImg = '/images/disable.gif';
                return ''
                + '<div style="text-align:center;height:13px;overflow:visible">'
                + '<img style="vertical-align:-3px" src="'
                + (val == 'yes' ? checkedImg : uncheckedImg)
                + '"'
                + ' />'
                + '</div>'
                    ;
            },
            disableRenderer: function (val) {
                var disabled = val;
                if (val == undefined || val == "")
                    disabled = "no";

                var checkedImg = '/images/enable.gif';
                var uncheckedImg = '/images/disable.gif';
                return ''
                + '<div style="text-align:center;height:13px;overflow:visible">'
                + '<img style="vertical-align:-3px" src="'
                + (disabled == 'no' ? checkedImg : uncheckedImg)
                + '"'
                + ' />'
                + '</div>'
                    ;
            },
            isValidObjectName: function (str) {
                return !(str.match(/^[0-9a-zA-Z]{1}([0-9a-zA-Z_-]|[ ]|[.])*$/) == null);
            },
            checkValidObjectName: function (name, objType) {
                var ret = Pan.base.isValidObjectName(name);
                if (!ret) {
                    var message = _T("A valid {type} object name must start with an alphanumeric character and can contain zero or more alphanumeric characters, underscore '_', hyphen '-', dot '.' or spaces.", {type: objType || ''});
                    Pan.Msg.alert("Error", message);
                }

                return ret;
            },
            showConfigError: function (a) {
                var errors = ["Configuration failed!<br/>"];
                if (a && a.result && a.result.errors) {
                    for (var i = 0; i < a.result.errors.length; i++) {
                        errors.push(Pan.base.htmlEncode(a.result.errors[i].line));
                    }
                }
                Pan.Msg.alert("Warning", errors.join("<br/>"));
            },
            setRolebaseAdminAccess: function (path, buttons) {
                // set role base admin
                var permission = Pan.rolebasePermission(path);
                var role = Pan.getUserRole();
                var isReadonly = permission == 'read-only' || role.indexOf('reader') != -1;
                var isVsysAdmin = Pan.isVsysAdmin();
                var networkDisable = isReadonly || isVsysAdmin;
                if (networkDisable) {
                    for (var i = 0; i < buttons.length; i++) {
                        buttons[i].setVisible(false);
                    }
                }
            },
            objectKeys: function (o, sort) {
                var keys = [];

                for (var key in o) {
                    if (o.hasOwnProperty(key)) {
                        keys.push(key);
                    }
                }
                if (sort) {
                    Pan.base.util.naturalSort(keys);
                }
                return keys;
            },
            /**
             * Recursively iterating over an object and calling callback on object and subobjects and subarrays
             * @param obj
             * @param callback
             * @param before
             *
             * if before.iterate is supplied, iteration will skip objects where before.iterate returns false
             * if before.callback is supplied, callback will not be called where before.callback returns false
             *
             * before {
         *      iterate: function(subobject, propertyname) {}
         *      callback: function(suboject, propertyname) {}
         * }
             */
            iterateObject: function (obj, callback, before) {
                var iter = function (obj, callback, before, fieldname) {
                    if (!before.callback || before.callback(obj, fieldname)) {
                        callback(obj);
                    }
                    var i;
                    if (Ext.isArray(obj)) {
                        for (i = 0; i < obj.length; i++) {
                            if (!before.iterate || before.iterate(obj[i], i)) {
                                iter(obj[i], callback, before);
                            }
                        }
                    } else if (Ext.isObject(obj)) {
                        for (i in obj) {
                            if (obj.hasOwnProperty(i)) {
                                if (!before.iterate || before.iterate(obj[i], i)) {
                                    iter(obj[i], callback, before, i);
                                }
                            }
                        }
                    }
                };
                before = before || {};
                if (typeof before == "function") {
                    var fn = before;
                    before = {callback: fn, validation: fn};
                }
                iter(obj, callback, before);
            },
            clone: function (obj) {
                var dup = function (obj) {
                    var rv = obj;
                    if (rv !== undefined && rv !== null) {
                        rv = obj.__clone__;
                        if (!Ext.isDefined(rv)) {
                            if (Ext.isArray(obj)) {
                                obj.__clone__ = rv = obj.slice(0);
                                for (var i = 0; i < rv.length; i++) {
                                    rv[i] = dup(rv[i]);
                                }
                            } else if (Ext.isObject(obj) && obj.__cloneable__ !== false) {
                                rv = {};
                                for (var m in obj) {
                                    if (obj.hasOwnProperty(m)) {
                                        rv[m] = dup(obj[m]);
                                    }
                                }
                                obj.__clone__ = rv;
                            } else {
                                rv = obj;
                            }
                        }
                    }
                    return rv;
                };
                var rv = dup(obj);
                var cleanup = function (obj) {
                    if (obj !== undefined && obj !== null) {
                        if (Ext.isDefined(obj.__clone__)) {
                            delete obj.__clone__;
                        }
                        if (Ext.isArray(obj)) {
                            for (var i = 0; i < obj.length; i++) {
                                cleanup(obj[i]);
                            }
                        } else if (Ext.isObject(obj) && obj.__cloneable__ !== false) {
                            for (var m in obj) {
                                if (obj.hasOwnProperty(m)) {
                                    cleanup(obj[m]);
                                }
                            }
                        }
                    }
                };
                cleanup(obj);
                return rv;
            },
            diff: util.diff,
            eachNumberedProperty: function (obj, func) {
                var keys = [];
                for (var n in obj) {
                    if (obj.hasOwnProperty(n)) {
                        if (typeof n == "number" || !isNaN(parseInt(n, 10))) {
                            keys.push(n);
                        }
                    }
                }
                keys.sort(function (x, y) {
                    var a = parseInt(x, 10), b = parseInt(y, 10);
                    return a > b ? 1 : a < b ? -1 : 0;
                });
                for (var i = 0; i < keys.length; i++) {
                    var rc = func.call(null, obj[keys[i]], keys[i], keys.length);
                    if (rc === false) {
                        break;
                    }
                }
            },
            encodeURIComponentPair: function (k, v) {
                return encodeURIComponent(k) + "=" + encodeURIComponent(v);
            },
            collectXmlTextNodes: function (x, separator) {
                if (!separator) {
                    separator = "\n";
                }
                if (x && x.documentElement) {
                    return Pan.base.collectXmlTextNodes(x.documentElement, separator);
                }
                if (x && x.nodeType == 3) { // XML TEXT
                    return Pan.base.htmlEncode(x.data);
                } else if (x && x.nodeType == 1) { // XML ELEMENT TYPE
                    var arr = [];
                    for (var i = 0; i < x.childNodes.length; i = i + 1) {
                        var result = Pan.base.collectXmlTextNodes(x.childNodes[i], separator);
                        if (result !== '') {
                            arr.push(result);
                        }
                    }
                    return arr.join(separator);
                } else {
                    return '';
                }
            },
            isMgmtSuccessResponse: function (root) {
                return Ext.DomQuery.selectNode('response[status=success]', root);
            },
            extractMgmtErrmsg: function (root) {
                var errmsg = [];
                var textnodes = function (nodename) {
                    var msgs = Ext.DomQuery.select(nodename, root);
                    for (var i = 0; i < msgs.length; i++) {
                        var n = msgs[i];
                        if (n.firstChild && n.firstChild.nodeType == 3)
                            errmsg.push(n.firstChild.nodeValue);
                    }
                };
                textnodes('line');
                textnodes('msg');
                textnodes('result');
                return (Pan.base.htmlEncode(errmsg).join('\n'));
            },
            isJsonSuccessResponse: function (root) {
                return Pan.base.json.path(root, '$.@status') == 'success';
            },
            extractJsonMsg: function (root) {
                if (!root) {
                    return '';
                }
                var msgs = [];
                var xpaths = ['$..line', '$..msg', '$..result', '$..member'];
                Ext.each(xpaths, function (xpath) {
                    var texts = jsonPath(root, xpath);
                    Ext.each(texts, function (text) {
                        if (Ext.isString(text)) {
                            msgs.push(Pan.base.htmlEncode(text));
                        } else if (Ext.isArray(text)) {
                            Ext.each(text, function (txt) {
                                if (Ext.isString(txt)) {
                                    msgs.push(Pan.base.htmlEncode(txt));
                                }
                            });
                        }
                    });
                });

                return (msgs.join('\n'));
            },
            extractJsonText: function (simpleObject) {
                var value, values;
                switch (Ext.type(simpleObject)) {
                    case 'object':
                        values = [];
                        for (var p in simpleObject) {
                            if (simpleObject.hasOwnProperty)
                                if (simpleObject.hasOwnProperty(p))
                                    if (p.indexOf('@') != 0) { // skip xml attributes @
                                        value = Pan.base.extractJsonText(simpleObject[p]);
                                        if (value !== false) {
                                            values.push(value);
                                        }
                                    }
                        }
                        return values.join("\n");
                    case 'array':
                        values = [];
                        for (var i = 0, len = simpleObject.length; i < len; i++) {
                            value = Pan.base.extractJsonText(simpleObject[i]);
                            if (value !== false) {
                                values.push(value);
                            }
                        }
                        return values.join("\n");
                    case 'number':
                        return '' + simpleObject;
                    case 'string':
                        return Pan.base.htmlEncode(simpleObject);
                    default:
                        return false;
                }
            },
            param: function (name) {
                var match = (new RegExp('[?&]' + name + '=([^&]*)')).exec(window.location.search);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            },
            urlencode: function (query, doseq) {
                var str = function (stuff) {
                    return '' + stuff;
                };
                var k, v;
                var l = [];
                if (!doseq) {
                    for (k in query) {
                        if (query.hasOwnProperty(k)) {
                            v = query[k];
                            k = encodeURIComponent(str(k));
                            v = encodeURIComponent(str(v));
                            l.push(k + '=' + v);
                        }
                    }
                } else {
                    for (k in query) {
                        if (query.hasOwnProperty(k)) {
                            v = query[k];
                            k = encodeURIComponent(str(k));
                            if (typeof v == 'string' || v.length == undefined) {
                                v = encodeURIComponent(v);
                                l.push(k + '=' + v);
                            } else {
                                var x = v.length;
                                for (var i = 0; i < x; i++) {
                                    var elt = v[i];
                                    l.push(k + '=' + encodeURIComponent(str(elt)));
                                }
                            }
                        }
                    }
                }
                return l.join('&');
            },
            escapeHTML: function (str) {
                var div = document.createElement('div');
                var text = document.createTextNode(str);
                div.appendChild(text);
                return div.innerHTML;
            },
            escapeXML: function (str) {
                // input might not be string
                str += '' + '';
                return str.replace(/&/g, '&amp;').replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;').replace(/'/g, '&apos;').replace(/"/g, '&quot;').replace(/'/gm, "&#39;");
            },
            serializeObject: function (_obj) {
                // Let Gecko browsers do this the easy way
                if (_obj && typeof _obj.toSource !== 'undefined' && typeof _obj.callee === 'undefined') {
                    return _obj.toSource();
                }
                // Other browsers must do it the hard way
                switch (typeof _obj) {
                    // numbers, booleans, and functions are trivial:
                    // just return the object itself since its default .toString()
                    // gives us exactly what we want
                    case 'number':
                    case 'boolean':
                    case 'function':
                        return _obj;
                    // for JSON format, strings need to be wrapped in quotes
                    case 'string':
                        return "'" + _obj.replace(/\\/, "\\\\").replace(/'/, "\\'") + "'";
                    case 'object':
                        var str;
                        if (_obj == null) return "null";
                        if (_obj.constructor === Array || typeof _obj.callee !== 'undefined') {
                            str = '[';
                            var i, len = _obj.length;
                            for (i = 0; i < len - 1; i++) {
                                str += Pan.base.serializeObject(_obj[i]) + ',';
                            }
                            str += Pan.base.serializeObject(_obj[i]) + ']';
                        } else {
                            str = '{';
                            var key;
                            for (key in _obj) {
                                if (_obj.hasOwnProperty(key)) {
                                    str += key + ':' + Pan.base.serializeObject(_obj[key]) + ',';
                                }
                            }
                            str = str.replace(/,$/, '') + '}';
                        }
                        return str;
                    default:
                        return "'" + typeof _obj + "'";
                    //return 'UNKNOWN'  + typeof _obj;
                }
            },
            genLabel: function (comp, v) {
                v = v || "";
                if (comp && comp.unitText) {
                    v += ' (' + comp.unitText + ')';
                }
                return v;
            },
            genHelpTip: function (comp, hidden) {
                var helpTip = comp && comp.helpTip;
                if (helpTip) {
                    helpTip = helpTip === true ? comp.helpstring : helpTip;
                    if (comp.columnConfig && comp.columnConfig.doHTMLEncode === false) {
                        //do nothing here
                    } else {
                        helpTip = Pan.base.htmlEncode(helpTip);
                    }
                    return "<span class='x-form-item-label-help" + (hidden ? " x-hide-display" : "") + "' ext:qtip='" + helpTip + "'" + (comp.helpTipWidth ? " ext:qwidth='" + comp.helpTipWidth : "") + ">&nbsp;</span>";
                }
                return false;
            },
            /**
             * Generate or get empty text.
             * This is not optimal.
             * Each type (i.e. number, numberRange, text) should have their own genEmptyText().
             * Currently there is a risk that one type logic leak to another one if they are used/specified incorrectly.
             * @param component
             * @param field
             * @param {bool} [doEncoding] Whether the empty text should be html encoded or not.
             * @returns {*}
             */
            genEmptyText: function (component, field, doEncoding) {
                var returnValue;
                if (component.rangeText === false) {
                    returnValue = '';
                }
                else if (component.emptyText) {
                    returnValue = component.emptyText;

                    // Homa: should do encoding but need to wait for such case
                    // returnValue = doEncoding? base.htmlEncode(component.emptyText) : component.emptyText;
                }
                else if (component.useHelpStringAsDisplay) {
                    // the ' ' is necessary because combo box emptyText cannot be the same as one of its value.
                    returnValue = (component.helpStrings ? component.helpStrings[field.defaultValue] : field.defaultValue) + ' ';

                    // Homa: should do encoding but need to wait for such case. 
                    // returnValue = component.helpStrings ? component.helpStrings[field.defaultValue] : field.defaultValue;
                    // returnValue = base.emptyTextFormat(doEncoding ? base.htmlEncode(returnValue) : returnValue);
                }
                else if (component.rangeText && component.rangeText !== true) {
                    // `c.rangeText` is non-empty string.
                    // Homa: Skip encoding as rangeText should not be html (and can contain `>`, `<`).
                    returnValue = component.rangeText;
                }
                else {
                    returnValue = base.emptyTextFormat(doEncoding ? base.htmlEncode(field.defaultValue) : field.defaultValue, component.minValue, component.maxValue);
                }

                return returnValue;
            },
            /**
             * Gets empty text based on defaultValue, min or max.
             * @param [defaultValue]
             * @param [min]
             * @param [max]
             * @returns {string}
             */
            emptyTextFormat: function(defaultValue, min, max) {
                var rv = '';
                if (Ext.isDefined(defaultValue)) {
                    // the ' ' is necessary because combo box emptyText cannot be the same as one of its value.
                    rv = defaultValue + ' ';
                }

                if (Ext.isDefined(min) && Ext.isDefined(max)) {
                    rv += '[' + min + ' - ' + max + ']';
                } else if (Ext.isDefined(min)) {
                    rv += '[>= ' + min + ']';
                } else if (Ext.isDefined(max)) {
                    rv += '[<= ' + max + ']';
                }

                return rv;
            },
            htmlEncode: function (object) {
                if (Ext.isArray(object)) {
                    Ext.each(object, function (value, index, array) {
                        array[index] = Pan.base.htmlEncode(value);
                    });
                    return object;
                } else if (Ext.isObject(object)) {
                    for (var property in object) {
                        if (object.hasOwnProperty(property)) {
                            var value = object[property];
                            object[property] = Pan.base.htmlEncode(value);
                        }
                    }
                    return object;
                } else if (Ext.isString(object)) {
                    return Ext.util.Format.htmlEncode(object);
                } else if (object === null || object === undefined) {
                    return object;
                } else {
                    return object;
                }
            },
            getStoreFilterCount: function (store) {
                return {
                    // __prefilterTotalCount is for live grid server side filter
                    totalCount: store.isLiveGrid ? store.__prefilterTotalCount || store.getTotalCount() : store.getSnapshot ? store.getSnapshot().length : (store.snapshot || store.data).length,
                    filteredCount: (store.isLiveGrid || store.supportLocalPaging) ? store.getTotalCount() || store.getCount() : store.getCount()
                };
            },
            getStoreFilterCountString: function (store) {
                var getStoreFilterCount = store.getStoreFilterCount || Pan.base.getStoreFilterCount;
                var result = getStoreFilterCount(store);
                /*            return _T("{filteredCount} item{plural}", {
                 plural: (result.filteredCount === 1) ? "" : "s",
                 filteredCount: result.filteredCount
                 });*/
                if (result.totalCount === result.filteredCount) {
                    return _T("{filteredCount} item{plural}", {
                        plural: (result.filteredCount === 1) ? "" : "s",
                        filteredCount: result.filteredCount
                    });
                } else {
                    return _T("{filteredCount} / {totalCount}", {
                        totalCount: result.totalCount,
                        filteredCount: result.filteredCount
                    });
                }
            },
            getStoreFilterCountTip: function (store, sm, fn) {
                var getStoreFilterCount = store.getStoreFilterCount || Pan.base.getStoreFilterCount;
                var result = getStoreFilterCount(store);
                var tips = [];
                if (result.filteredCount != result.totalCount) {
                    tips.push(_T("{filteredCount} matched", {
                        filteredCount: result.filteredCount
                    }));
                }
                tips.push(_T("{totalCount} total", {
                    totalCount: result.totalCount
                }));
                var totalSelected,
                    inStoreSelectedTotal,
                    totalSelectedTip,
                    resultCount;
                if (sm || Ext.isFunction(fn)) {
                    if (sm) {
                        if (sm.getResultCount) {
                            resultCount = sm.getResultCount();
                            totalSelected = resultCount.total;
                            inStoreSelectedTotal = resultCount.inStoreTotal;
                        } else {
                            totalSelected = sm.getCount();
                        }
                    } else {
                        resultCount = fn();
                        totalSelected = resultCount.total;
                        inStoreSelectedTotal = resultCount.inStoreTotal;
                    }
                    totalSelectedTip = _T("{totalSelected} selected", {
                        totalSelected: totalSelected
                    });

                    if (Ext.isDefined(inStoreSelectedTotal) && inStoreSelectedTotal != totalSelected) {
                        totalSelectedTip += " (" + _T("{totalShown} shown", {
                            totalShown: inStoreSelectedTotal
                        }) + ")";
                    }

                    tips.push(totalSelectedTip);
                }
                return Pan.base.htmlEncode(tips).join("<br>");
            },
            safeHtmlEncode: function (object) {
                if (Ext.isArray(object)) {
                    Ext.each(object, function (value, index, array) {
                        array[index] = Pan.base.safeHtmlEncode(value);
                    });
                    return object;
                } else if (Ext.isObject(object)) {
                    for (var property in object) {
                        if (object.hasOwnProperty(property)) {
                            var value = object[property];
                            object[property] = Pan.base.safeHtmlEncode(value);
                        }
                    }
                    return object;
                } else if (Ext.isString(object)) {
                    var results = [];
                    var whitelist = ["a", "abbr", "acronym", "b", "basefont", "big", "blockquote", "br", "button", "center", "cite", "code", "col", "dd", "div", "dl", "dt", "em", "fieldset", "font", "form", "hr", "i", "img", "input", "label", "li", "menu", "ol", "p", "pre", "q", "s", "samp", "select", "small", "span", "strike", "strong", "sub", "sup", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "tr", "tt", "u"];
                    HTMLParser(object, {
                        start: function (tag, attrs, unary) {
                            tag = tag.toLowerCase();
                            if (whitelist.indexOf(tag) >= 0) {
                                results.push("<" + tag);
                                for (var i = 0; i < attrs.length; i++) {
                                    var attributeName = attrs[i].name.toLowerCase();
                                    var attributeValue = attrs[i].escaped.toLowerCase();
                                    switch (attributeName) {
                                        case 'style': // style are unsafe, Internet Explorer can evaluation expressions and such
                                            break;
                                        case 'href':  // cannot run script
                                            if (attributeValue.indexOf('script') == -1) {
                                                results.push(" " + attrs[i].name + '="' + attrs[i].escaped + '"');
                                            }
                                            break;
                                        // case 'src': combined in default
                                        default:
                                            // onerror etc are unsafe
                                            if (attributeName.indexOf('on') != 0) {
                                                // cannot refer to external images or run script
                                                if (attributeValue.indexOf('http') == -1 &&
                                                    attributeValue.indexOf('script') == -1) {
                                                    results.push(" " + attrs[i].name + '="' + attrs[i].escaped + '"');
                                                }
                                            }
                                            break;
                                    }
                                }
                                results.push((unary ? "/" : "") + ">");
                            }
                        },
                        end: function (tag) {
                            tag = tag.toLowerCase();
                            if (whitelist.indexOf(tag) >= 0) {
                                results.push("</" + tag + ">");
                            }
                        },
                        chars: function (text) {
                            results.push(Ext.util.Format.htmlEncode(text));
                        },
                        comment: function (text) {
                            // results.push("<!--" + text + "-->");
                        }
                    });
                    return results.join('');
                } else {
                    return object;
                }
            },
            serverTokenGen: function (st) {
                var cookie;
                for (var m in st.st) {
                    if (st.st.hasOwnProperty(m)) {
                        cookie = Pan.st.st[m];
                        break;
                    }
                }
                return function (tid) {
                    if (!Ext.isDefined(tid)) {
                        var t = new Ext.Direct.Transaction();
                        tid = t.tid;
                    }
                    return {
                        '___token': /*SHA256*/ MD5(cookie + tid),
                        '___tid': tid
                    };
                };
            },

            dom2array: function ($node, $safeAttribute, $replaceDashes, $options) {
                var $idx = 0;
                var $res = {};
                var $children = null, $i = null, $child = null;
                var $len;
                if (isTxtNode($node)) {
                    $res.push($node.nodeValue);
                } else if ($node.nodeType == 1) {
                    var $alwaysCreateArray = false;
                    if ($node.hasAttributes()) {
                        var $attributes = $node.attributes;
                        if ($attributes) {
                            var $keepSrcAttribute = false;
                            if ($options && $options['keepSrcAttribute']) {
                                $keepSrcAttribute = $options['keepSrcAttribute'];
                            } else if ($options && $options['keepSrcAttributeIfIsElement'] && $options['keepSrcAttributeIfIsElement'] === true) {
                                $alwaysCreateArray = true;
                                if ($node.hasChildNodes()) {
                                    $children = $node.childNodes;
                                    for ($i = 0; $i < $children.length; $i++) {
                                        $child = $children.item($i);
                                        if ($child.nodeType == 1) {
                                            $keepSrcAttribute = true;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                $keepSrcAttribute = $node.hasAttribute('__recordInfo');
                            }
                            for ($i = 0, $len = $attributes.length; $i < $len; ++$i) {
                                var $attr = $attributes.item($i);
                                if ($attr.name == "dirty" ||
                                    $attr.name == "admin" ||
                                    $attr.name == "dirtyId" ||
                                    $attr.name == "minver" ||
                                    $attr.name == "maxver" ||
                                    $attr.name == "platform" ||
                                    $attr.name == "time") {
                                    continue;
                                }

                                // chop all non element container's src attribute
                                if (!$keepSrcAttribute && ($attr.name == "src")) {
                                    continue;
                                }

                                var $attrName = '@' + $attr.name;
                                $attrName = $safeAttribute ? '_' + $attrName : $attrName;
                                $attrName = $replaceDashes ? $attrName.replace('-', '_') : $attrName;
                                $res[$attrName] = $attr.value;
                            }
                        }
                    }
                    if ($node.hasChildNodes()) {
                        $children = $node.childNodes;
                        var $is_array_children = false;
                        var $firstChild = $children.item(0);

                        if ($firstChild.nodeType == 1) {
                            if ($firstChild.nodeName == "entry" ||
                                $firstChild.nodeName == "member" ||
                                $firstChild.nodeName == "completion") {
                                $is_array_children = true;
                            } else if ($children.length > 1) {
                                var $secondChild = $children.item(1);
                                $is_array_children = ($secondChild.nodeType == 1) &&
                                ($firstChild.nodeName == $secondChild.nodeName);
                            }
                        }
                        for ($i = 0; $i < $children.length; $i++) {
                            $child = $children.item($i);
                            if ($child.nodeType == 1) {
                                if ($is_array_children) {
                                    // removed below if/else for logsettings system
                                    if (/*$child.nodeName != "completion"*/$child.nodeName == "member" || $child.nodeName == "entry" || $alwaysCreateArray) {
                                        if (!$res[$child.nodeName]) {
                                            $res[$child.nodeName] = [];
                                        }
                                        $res[$child.nodeName].push(Pan.base.dom2array($child, $safeAttribute, $replaceDashes, $options));
                                    } else {
                                        $res[$idx++] = Pan.base.dom2array($child, $safeAttribute, $replaceDashes, $options);
                                    }
                                } else {
                                    var $nodeName = $replaceDashes ? $child.nodeName.replace("-", "_") : $child.nodeName;
                                    $res[$nodeName] = Pan.base.dom2array($child, $safeAttribute, $replaceDashes, $options);
                                }
                            } else if (isTxtNode($child)) {
                                if (Pan.base.isEmpty($res) && $children.length === 1) {
                                    $res = $child.nodeValue;
                                } else {
                                    //$res['text'] = $child.nodeValue;
                                }
                            }
                        }
                    }
                }
                return $res;
            }
        };
        //umd.browserGlobal.define(base, "Pan.base");
        return base;
    });
}, "Pan.base", require, exports, module);

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 * vim: sw=4 ts=4 expandtab
 */