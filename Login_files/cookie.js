/*!
 * PAN Management UI
 * Copyright(c) Palo Alto Networks, Inc.
 */
Ext.ns('Pan', 'Pan.base', 'Pan.base.cookie');
Ext.apply(Pan.base.cookie, {
    set: function(name, value, days, path) {
        var expires = -1;
        if (typeof days == "number" && days >= 0) {
            var d = new Date();
            d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = d.toGMTString();
        }
        value = escape(value);
        document.cookie = name + "=" + value + ";"
                + (expires != -1 ? " expires=" + expires + ";" : "")
                + "path=" + (path || "/");
    },

    get: function(name) {
        var idx = document.cookie.indexOf(name + '=');
        if (idx == -1) {
            return null;
        }
        var value = document.cookie.substring(idx + name.length + 1);
        var end = value.indexOf(';');
        if (end == -1) {
            end = value.length;
        }
        value = value.substring(0, end);
        value = unescape(value);
        return value;
    },

    remove: function(name) {
        this.set(name, "-", 0);
    },

    setObject: function(name, obj, days, path, clearCurrent) {
        var pairs = [], cookie, value = "";
        if (!clearCurrent) {
            cookie = this.getObject(name);
        }
        if (days >= 0) {
            if (!cookie) {
                cookie = {};
            }
            var prop;
            for (prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    if (prop == null) {
                        delete cookie[prop];
                    } else if (typeof obj[prop] == "string" || typeof obj[prop] == "number") {
                        cookie[prop] = obj[prop];
                    }
                }
            }
            for (prop in cookie) {
                if (cookie.hasOwnProperty(prop)) {
                    pairs.push(escape(prop) + "=" + escape(cookie[prop]));
                }
            }
            value = pairs.join("&");
        }
        this.set(name, value, days, path);
    },

    getObject: function(name) {
        var values = null, cookie = this.get(name);
        if (cookie) {
            values = {};
            var pairs = cookie.split("&");
            for (var i = 0; i < pairs.length; i++) {
                var pair = pairs[i].split("=");
                var value = pair[1];
                if (isNaN(value)) {
                    value = unescape(pair[1]);
                }
                values[ unescape(pair[0]) ] = value;
            }
        }
        return values;
    },

    isCookieSupported: function() {
        if (typeof navigator.cookieEnabled != "boolean") {
            this.set("__TestingYourBrowserForCookieSupport__", "CookiesAllowed", 90, null);
            var cookieVal = this.get("__TestingYourBrowserForCookieSupport__");
            navigator.cookieEnabled = (cookieVal == "CookiesAllowed");
            if (navigator.cookieEnabled) {
                // FIXME: should we leave this around?
                this.deleteCookie("__TestingYourBrowserForCookieSupport__");
            }
        }
        return navigator.cookieEnabled;
    }
});
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 * vim: sw=4 ts=4 expandtab
 */
