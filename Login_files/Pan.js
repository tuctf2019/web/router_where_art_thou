/*
 * Copyright(c) 2015, Palo Alto Networks, inc.
 * Created by hwong on 2/12/15.
 * When creating singleton-instance-as-property in the constructor, don't use helper function such as Ext.apply(...)
 * This is because the singleton instance's constructor might be messy and make additional calls to the backend server
 * as well as create side effects. I want to minimize that and only making new instance when it is needed.
 */

umd(function (define) {
    define(function (require /*, exports, module */) {
        var _ = require('underscore');

        /**
         * Copy all properties in the `source` to the `target` and injects properties defined in the `options` to the target.
         * New object literals will be created along the object paths defined in the `options`. This is done to prevent side effect of affecting
         * the `source` when one of the property is replaced. This is specifically useful when the `source` is static or singleton.
         * @example
         * source = { a: { b: 1, c: 2 }}, options = { a: { b: 3}}
         * then target = { a: { b: 3, c: 2}} while source remains the same. This can happen only if target.a !== source.a,
         * meaning target.a is a new object literal.
         * @param {object} target - The target is typically the `this` object in the constructor.
         * @param {object} source - The source where the most properties are copied from.
         * @param {...object} options - option to inject with.
         * @returns {*|{}}
         */
        function inject(target, source, options) {
            if (arguments.length === 2) {
                options = arguments[1];
                source = arguments[0];
                target = {};
            }

            var name;
            // Copy source to target
            for (name in source) {
                if (source.hasOwnProperty(name)) {
                    target[name] = source[name];
                }
            }

            for (name in options) {
                if (options.hasOwnProperty(name)) {
                    var property = options[name];
                    if (property === null) {
                        continue;
                    }

                    if (_.isObject(property) && !_.isFunction(property)) {
                        // target = { a: { b: 1, c: 2}}
                        // options = { a: { c: 3}}
                        target[name] = inject(target[name], property);
                    }
                    else {
                        target[name] = property;
                    }
                }
            }

            // Return the modified object
            return target;
        }

        function Pan(options) {
            inject(this, window.Pan, options);
        }

        return Pan;
    });
}, 'Pan', require, exports, module);
