/*!
 * PAN Management UI
 * Copyright(c) Palo Alto Networks, Inc.
 */
umd(function (define) {
    define(function (require /*, exports, module */) {
        var util = {
            findComponentWithProperty: function (container, prop) {
                return container.findBy(function (c) {
                    if (Ext.isDefined(c[prop])) {
                        return true;
                    }
                });
            },
            findOwnerCtBy: function (obj, func) {
                while (obj) {
                    if (func(obj)) {
                        return obj;
                    }
                    obj = obj.ownerCt;
                }
                return null;
            },
            findOwnerCtByXtype: function (child, xtype) {
                return util.findOwnerCtBy(child, function (obj) {
                    return obj.constructor && obj.constructor.xtype == xtype;
                });
            },
            xmlEncode: function (value) {
                return !value ? value : String(value).replace(/&/g, '&amp;').replace(/>/g, '&gt;').replace(/</g, '&lt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
            },

            getNumericSuffix: function (number) {
                if (isNaN(number)) {
                    return "";
                }
                number = Math.abs (parseInt (number, 10));
                switch (number % 10) {
                    case 1: return _T("st");
                    case 2: return _T("nd");
                    case 3: return _T ("rd");
                    default: return _T("th");
                }

                return "";
            },

            xmlFormat: function (xml) {
                var formatted = '';
                var reg = /(>)(<)(\/*)/g;
                xml = xml.replace(reg, '$1\r\n$2$3');
                var pad = 0;
                jQuery.each(xml.split('\r\n'), function (index, node) {
                    var indent = 0;
                    if (node.match(/.+<\/\w[^>]*>$/)) {
                        indent = 0;
                    } else if (node.match(/^<\/\w/)) {
                        if (pad != 0) {
                            pad -= 1;
                        }
                    } else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
                        indent = 1;
                    } else {
                        indent = 0;
                    }
                    var padding = '';
                    for (var i = 0; i < pad; i++) {
                        padding += '  ';
                    }
                    formatted += padding + node + '\r\n';
                    pad += indent;
                });
                return formatted;
            },
            selectiveApply: function (listOfProperties, o, c, defaults) {
                if (defaults) {
                    util.selectiveApply(listOfProperties, o, defaults);
                }
                if (o && c && typeof c == 'object') {
                    for (var p in c) {
                        if (c.hasOwnProperty(p)) {
                            if (listOfProperties.indexOf(p) >= 0) {
                                o[p] = c[p];
                            }
                        }
                    }
                }
                return o;
            },
            selectiveApplyIf: function (listOfProperties, o, c) {
                if (o) {
                    for (var p in c) {
                        if (c.hasOwnProperty(p)) {
                            if (!Ext.isDefined(o[p]) && listOfProperties.indexOf(p) >= 0) {
                                o[p] = c[p];
                            }
                        }
                    }
                }
                return o;
            },
            capitalize: function (val) {
                val = val || '';
                val = val.replace(/-/g, ' ');
                val = val.replace(/^[@]/g, '');
                return val.replace(/(^|\s)([a-z])/g, function (m, p1, p2) {
                    return p1 + p2.toUpperCase();
                });
            },
            decapitalize: function (val) {
                val = val || '';
                return val.replace(/ /g, '-').toLowerCase();
            },
            prettyPrintNumber: function (count, base, decimalPlaces) {
                if (!decimalPlaces) decimalPlaces = 1;
                if (!base) {
                    base = 1000;
                }
                if (count < base) {
                    return count;
                }
                var units = ['k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'];
                for (var i = 0; i < units.length; i++) {
                    if (count < Math.pow(base, i + 2)) {
                        break;
                    }
                }
                if (i >= units.length) {
                    i = units.length - 1;
                }
                return (count / (0.0 + Math.pow(base, i + 1))).toFixed(decimalPlaces) + units[i];
            },
            numberFormat: function (v /*, metadata, record, rowIndex, colIndex, store*/) {
                var whole = String(v);
                var r = /(\d+)(\d{3})/;
                while (r.test(whole)) {
                    whole = whole.replace(r, '$1' + ',' + '$2');
                }
                return whole;
            },
            // Will check for the Uniqueness
            contains: function (a, e) {
                for (var j = 0; j < a.length; j++) {
                    if (a[j] == e) {
                        return true;
                    }
                }
                return false;
            },
            // remove duplicate from an array
            unique: function (a) {
                var temp = [];
                for (var i = 0; i < a.length; i++) {
                    if (!util.contains(temp, a[i])) {
                        temp.length += 1;
                        temp[temp.length - 1] = a[i];
                    }
                }
                return temp;
            },
            /**
             * Creates an extension function. The passed function, func, is called. The superFunc can be accessed within func.
             * The passed function is called with the parameters of the original function. Example usage:
             * <pre><code>
             var sayHi = function(name){
    alert('Hi, ' + name);
     };

             sayHi('Fred'); // alerts "Hi, Fred"

             // create a new function that validates input without
             // directly modifying the original function:

             var znwFunction = function(name){
    if (name == 'znw') {
        return "Zhi-Ning Wang"
     } else {
     return znwFunction.superFunction(name);
     }
     };

             var sayHiToFriend = util.createExtension(znwFunction, sayHi);

             sayHiToFriend('znw');  // alerts "Zhi-Ning Wang"
             sayHiToFriend('Brian'); // alerts "Hi, Brian"
             </code></pre>
             * @param {Function} func The function to call
             * @param {Function} superFunc The super function that could be used in func
             * @param {Object} [scope] The scope (<code><b>this</b></code> reference) in which the passed function is executed.
             * <b>If omitted, defaults to the scope in which the original function is called or the browser window.</b>
             * @return {Function} The new function
             */
            createExtension: function (func, superFunc, scope) {
                var rv = function () {
                    var me = scope || this || window;
                    return func.apply(me, arguments);
                };
                func.scope = scope;
                func.superFunction = superFunc;
                rv.scope = scope;
                rv.superFunction = superFunc;
                return rv;
            },

            /*
             return util.createFirstIntercept(function() {
             var grid = arguments[arguments.length - 1];
             grid.addListener('cellclick', function() {
             var a;
             });
             return fn.apply(this, arguments);
             }, fn);
             */
            createFirstIntercept: function (firstInterceptFunc, nthTimeFunc, scope) {
                var func = firstInterceptFunc;
                return function () {
                    var that = scope || this || window;
                    var result = func.apply(that, arguments);
                    func = nthTimeFunc;
                    return result;
                };
            },

            // provide a conditional extend of a superClass, userSuperClass is a string in the constructor config which
            // indicates whether to use the super class (true) or bypass it (false)

            /**
             * Conditional extend of a superClass.
             */
            cextend: function () {
                var oc = Object.prototype.constructor;

                var isUseSuperClass = function (useSuperClass, scope, config) {
                    if (Ext.isBoolean(useSuperClass)) {
                        return useSuperClass;
                    } else if (Ext.isFunction(useSuperClass)) {
                        return useSuperClass.call(scope);
                    } else if (Ext.isString(useSuperClass)) {
                        if (Ext.isObject(config)) {
                            if (Ext.isBoolean(config[useSuperClass])) {
                                return config[useSuperClass];
                            }
                        }
                        var m = scope[useSuperClass];
                        if (m != undefined) {
                            return isUseSuperClass(m, scope, undefined);
                        }
                    }
                    //throw "util.cextend: useSuperClass not set properly";
                    return false;
                };

                return function (ownClass, superClass, useSuperClass, overrides) {
                    // reassign arguments when necessary (Ext.extend style)
                    if (Ext.isObject(useSuperClass)) {
                        overrides = useSuperClass;
                        useSuperClass = superClass;
                        superClass = ownClass;
                        ownClass = overrides.constructor != oc ? overrides.constructor : null;
                    }

                    // The ctor is the constructor for our cextended.  It's job is to check during instantiation the useSuperClass
                    // flag, and assign it in the this pointer
                    var ctor = function (config) {
                        var me = arguments.callee;
                        this.__isUseSuperClass = isUseSuperClass(me.useSuperClass, this, config);
                        var callFunction = me.ownClass;
                        if (!callFunction) {
                            if (!this.__isUseSuperClass) {
                                callFunction = me.superClass.superclass.constructor;
                            } else {
                                callFunction = me.superClass;
                            }
                        }
                        return callFunction.apply(this, arguments);
                    };
                    ctor.superClass = superClass; // eg: Ext.ux.grid.livegrid.GridPanel
                    ctor.ownClass = ownClass; // eg: Pan.base.grid.GridPanel
                    ctor.useSuperClass = useSuperClass;

                    // loop through the superClass.prototype (eg: livegrid.GridPanel) to put in "man in the middle", which
                    // will switch between superFunction or superFunction.superFunction.
                    var hasSeenConstructor = false;
                    for (var m in superClass.prototype) {
                        // for IE, constructor is not part of m!
                        if (m === 'constructor') {
                            hasSeenConstructor = true;
                        }
                        util.setupCExtendIntercept(superClass, m);
                    }

                    // for IE, constructor is not part of m!  Need to do it manually
                    if (!hasSeenConstructor) {
                        util.setupCExtendIntercept(superClass, 'constructor');
                    }

                    var extExtendResult = Ext.extend(ctor, superClass, overrides);
                    // the extExtendResult.prototype.constructor is the one used in derived constructor
                    // eg Pan.base.grid.GridPanel.superclass.constructor.apply(this, arguments)
                    // hence, it needs to point to our new intercepted constructor
                    extExtendResult.prototype.constructor = extExtendResult;
                    return extExtendResult;
                };
            }(),

            setupCExtendIntercept: function (superClass, m) {
                if (superClass.prototype.hasOwnProperty(m)) {
                    var original = superClass.prototype[m];
                    if (Ext.isFunction(original)) {
                        //This is the "man in the middle" function
                        var newFunc = function () {
                            var me = arguments.callee;
                            if (this.__isUseSuperClass) {
                                return me.superFunction.apply(this, arguments);
                            } else {
                                //noinspection JSUnresolvedVariable
                                if (me.superFunction.superFunction) {
                                    //noinspection JSUnresolvedVariable
                                    return me.superFunction.superFunction.apply(this, arguments);
                                } else {
                                    throw "util.cextend: superFunction.superFunction does not exist!";
                                }
                            }
                        };
                        newFunc.superFunction = original;
                        newFunc.superFunction.superFunction = superClass.superclass[m];
                        superClass.prototype[m] = newFunc;
                    }
                }
            },

            invokeLater: function (delayInMilliSeconds, func, scope, args) {
                if (!delayInMilliSeconds) {
                    delayInMilliSeconds = 0;
                }
                var task = new Ext.util.DelayedTask(func, scope, args);
                task.delay(delayInMilliSeconds);
            },

            yes2TrueFn: function (value) {
                return value === "yes";
            },

            // combines entry into master array.  Takes care of all situations
            integrateArray: function (master, entry) {
                if (master) {
                    if (!Ext.isArray(master)) {
                        master = [master];
                    } else {
                        master = master.slice(0); // in case it is from prototype
                    }
                    if (Ext.isArray(entry)) {
                        for (var i = 0; i < entry.length; i++) {
                            master.push(entry[i]);
                        }
                    } else if (entry) {
                        master.push(entry);
                    }
                } else if (Ext.isArray(entry)) {
                    master = entry.slice(0);
                } else {
                    master = [entry];
                }
                return master;
            },

            getFileNameFromFullPath: function (fullPath) {
                // we need to fill customUrlCategoryFileC with just the file name part of the file.
                var theFileName = '';
                var fileName = Ext.util.Format.trim(fullPath);
                if (fileName.length === 0)
                    return theFileName;

                var idx = fileName.lastIndexOf("/");
                if (idx >= 0) {
                    // found a forward slash, portion after last slash is fileName
                    theFileName = fileName.substring(idx + 1);
                } else {
                    idx = fileName.lastIndexOf("\\");
                    if (idx >= 0) {
                        // found a backward slash, portion after last slash is fileName
                        theFileName = fileName.substring(idx + 1);
                    } else {
                        // no forward or backward slashes, assume the whole thing is fileName
                        theFileName = fileName;
                    }
                }
                return theFileName;
            },

            openModalWindow: function (url, name, width, height) {

                var newWin;
                if (window.showModalDialog) {
                    // NOTE: pass the window as the second parameter so that we can retrieve it in
                    // the opened window.
                    var myObject = {};
                    myObject.openerWindow = window;
                    myObject.windowName = name;
                    try {
                        window.showModalDialog(url, myObject, "dialogHeight:" + height + " px;dialogWidth:" + width + " px;center:yes;resizable:yes;scroll:yes;status:no;help:no;edge:sunken;unadorned:yes");
                    } catch (e) {
                        alert("Cannot open popup dialog. Please check browser settings.");
                    }
                } else {
                    newWin = window.open(url, name, "height=" + height + ",width=" + width + ",center=yes,resizable=yes,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=yes,modal=yes");
                    if (!newWin) {
                        alert("Cannot open popup dialog. Please check browser settings.");
                        return;
                    }
                    newWin.focus();
                }
            },

            stripHtmlAndEncode: function(data) {
                if (Ext.isArray(data)) {
                    var lines = data.slice(0);
                    for (var i = 0; i < lines.length; i++) {
                        // objects are not supported
                        if (Ext.isString(lines[i])) {
                            var line = Pan.base.htmlEncode(((lines[i]).replace(/<[^>]*>/g, '\n')));
                            lines[i] = line.trim().replace(/\n\s*/g, ' ');
                        } else {
                            lines[i] = "";
                        }
                    }
                    return lines;
                } else if (Ext.isString(data)) { //if it is not an array, assume it is single line; objects are not supported
                    return Pan.base.util.stripHtmlAndEncode([data])[0];
                } else if (Ext.isObject(data)) {
                    return "";
                } else {
                    return data;
                }
            },

            array2HTMLList: function (data, listStyle) {
                var lines = [];
                if (Ext.isArray(data)) {
                    lines = data;
                } else if (data) { //if it is not an array, assume it is single line
                    lines = [data];
                }

                var htmlList = '<ul>';
                for (var i = 0; i < lines.length; i++) {
                    if (Ext.isEmpty(lines[i]))
                        htmlList += '<li/>';
                    else if (lines.length == 1 || lines[i].indexOf('<div>') == 0 || lines[i].trim().length == 0)
                        htmlList += '<li>' + lines[i] + '</li>';
                    else {
                        htmlList += '<li>';
                        if (listStyle !== "none") {
                            htmlList += '<b>. </b>';
                        }
                        htmlList += lines[i] + '</li>';
                    }
                }
                htmlList += '</ul>';
                return htmlList;
            },

            array2IndentedHTMLList: function (data) {
                if (Ext.isArray(data) && data.length == 1) {
                    return '<ul><li><b>. </b>' + data[0] + '</li></ul>';
                }
                return util.array2HTMLList(data);
            },

            splitByLength: function (str, length, separator) {
                if (str && str.length > length) {
                    if (!Ext.isDefined(separator)) {
                        separator = "\n";
                    }
                    var collection = [];
                    while (str.length > length) {
                        collection.push(str.substr(0, length));
                        str = str.substr(length);
                    }
                    if (str.length > 0) {
                        collection.push(str);
                    }
                    return collection.join(separator);
                } else {
                    return str;
                }
            },
            // Returns the number of seconds since midnight Jan 1, 1970
            // Takes the same arguments as Date constructor
            epoch: function (arg) {
                var newDateLong = function (year, month, date, hour, minute, second, millisecond) {
                    return new Date(year, month, date, hour, minute, second, millisecond);
                };
                var d;
                if (arguments.length > 1) {
                    d = newDateLong.apply(null, a);
                } else if (arguments.length == 1) {
                    d = new Date(arg);
                } else {
                    d = new Date();
                }
                return Math.round(d.getTime() / 1000.0);
            },

            // This is for sorting objects in array according to a property or dot-formation list of properties
            // in these objects. For example, we can pass [(p : 'value1'), (p : 'value2')] as arr, and 'p' as
            // sortProperty. This also supports passing dot-formation such as sortProperty = 'data.full-name'.
            naturalSortObjectArray: function (arr, sortProperty) {
                if (arr && arr.sort && sortProperty) {
                    arr.sort(function(a, b) {
                        var ca = util.jsonPathOutputToString(a, sortProperty);
                        var cb = util.jsonPathOutputToString(b, sortProperty);
                        return util.naturalSortCmp.call(null, ca, cb);
                    });
                }
            },

            jsonPathOutputToString: function (obj, sortProperty) {
                var v = jsonPath(obj, '$.' + sortProperty)[0];
                if (_.isNumber(v)) {
                    return v.toString();
                } else if (v) {
                    return v;
                } else {
                    return '';
                }
            },

            naturalSort: function (arr) {
                arr.sort(util.naturalSortCmp);
            },

            naturalSortCmp: function (a, b) {
                a = util.stringSortTypeFunction(a);
                b = util.stringSortTypeFunction(b);
                if (a === b) {
                    return 0;
                } else if (a < b) {
                    return -1;
                } else {
                    return +1;
                }
            },

            stringSortTypeFunction: function (s) {
                var a = s.toLowerCase().match(/(\d+|[^\d]+)/g);
                if (a && a.length > 0) {
                    var index = 0;
                    if (isNaN(parseInt(a[0], 10))) {
                        index++;
                    }
                    for (var i = index; i < a.length; i++, i++) {
                        a[i] = util.padLeadingString(a[i], 10, "0000000000");
                    }
                    return a.join("");
                }
                return s;
            },

            // Pad with leading string such as "000".  If leading string plus the original string is longer
            // than len, the chopping of leading string starts from the beginning
            padLeadingString: function (n, len, padString) {
                var s = n.toString();
                if (s.length < len) {
                    s = (padString + s).slice(-len);
                }

                return s;
            },

            /**
             * example usage: var foo = {a: "bar", b: "cat", c: [1, 2, 3, {some: "rat"}],
        d: [4,5,6], e: {a: "bar1", b: "cat1"}, f: [{a: "bar2", b: "cat2"}]};
             util.getLeafNodes(foo);
             * @param o
             * @param withKey
             * @return {Array}
             */
            getLeafNodes: function (o, withKey) {
                var leaves = [];

                function js_traverse(obj, key) {
                    var type = typeof obj;
                    if (type == "object") {
                        for (var n in obj) {
                            if (obj.hasOwnProperty(n)) {
                                js_traverse(obj[n], n);
                            }
                        }
                    } else {
                        if (withKey == true) {
                            leaves.push([key, obj]);
                        } else {
                            leaves.push(obj);
                        }
                    }
                }

                js_traverse(o);
                return leaves;
            },

            findRootOwnerCt: function (component) {
                var ownerCt = component;
                while (ownerCt.ownerCt) {
                    ownerCt = ownerCt.ownerCt;
                }
                return ownerCt;
            },

            needWrapParenthesis: function (str) {
                var len = str.length, i, c;
                if (!len) {
                    return false;
                }

                // str starts with (
                var count = 0;
                var hasp = false;
                for (i = 0; i < len; i++) {
                    c = str.charAt(i);
                    if (c == '(') {
                        count++;
                        hasp = true;
                    } else if (c == ')') {
                        count--;
                        hasp = true;
                    }
                    if (count == 0 && i != len - 1 && hasp) {
                        return true;
                    }
                }
                return false;
            },

            convertToString: function (obj) {
                var exp;
                if (obj['k'] != undefined && obj['op'] != undefined && obj['v'] != undefined) {
                    exp = ['(', obj['k'], ' ', obj['op'], ' \'', obj['v'], '\')'].join('');
                    return !obj.__negate ? exp : 'not ' + exp;
                }

                if (obj.l && obj.r) {
                    exp = [
                        '(',
                        util.convertToString(obj.l),
                        ' or ',
                        util.convertToString(obj.r),
                        ')'
                    ].join('');
                    return !obj.__negate ? exp : 'not ' + exp;
                }

                if (Ext.isArray(obj)) {
                    var arr = [];
                    for (var i = 0; i < obj.length; i++) {
                        arr.push(util.convertToString(obj[i]));
                    }
                    return !obj.__negate ? arr.join(' and ') : 'not (' + arr.join(' and ') + ')';
                }
            },

            /**
             * translate (A or B) and !B into A and !B
             * @param obj
             * @return {*}
             */
            optimize: function (obj) {
                if (obj['k']) return obj;
                if (obj.l && obj.r) {
                    return {
                        l: util.optimize(obj.l),
                        r: util.optimize(obj.r)
                    };
                }
                var changed = false;
                if (Ext.isArray(obj)) {
                    var i, j, len = obj.length;
                    for (i = 0; i < len; i++) {
                        if (obj[i].__negate == true) {
                            for (j = len - 1; j >= 0; j--) {
                                if (i != j) {
                                    var left = obj[j].l;
                                    var right = obj[j].r;
                                    if (left) {
                                        left.__negate = true;
                                        if (JSON.stringify(left) == JSON.stringify(obj[i])) {
                                            delete left.__negate;
                                            delete obj[j].l;
                                            obj[j] = obj[j].r;
                                            changed = true;
                                        } else {
                                            delete left.__negate;
                                        }
                                    }
                                    if (right) {
                                        right.__negate = true;
                                        if (JSON.stringify(right) == JSON.stringify(obj[i])) {
                                            delete right.__negate;
                                            delete obj[j].r;
                                            obj[j] = obj[j].l;
                                            changed = true;
                                        } else {
                                            delete right.__negate;
                                        }
                                    }
                                    if (!obj[j]) {
                                        obj.splice(j, 1);
                                    }
                                }
                            }
                        }
                    }
                }
                if (changed) {
                    return util.optimize(obj);
                } else {
                    return obj;
                }
            },

            textWidthCalculator: function (el) {
                var cellTextWidthMap = {};
                cellTextWidthMap[32] = el.getTextWidth("&#160;");
                for (var i = 33; i < 127; i++) {
                    cellTextWidthMap[i] = el.getTextWidth(String.fromCharCode(i));
                }

                var stripDisplayNone = function (html) {
                    if (Ext.isString(html)) {
                        var end, start = html.indexOf('<span style="display:none">');
                        if (start != -1) {
                            end = html.indexOf('</span>', start);
                            if (end != -1) {
                                html = html.substr(0, start) + html.substr(end);
                            }
                        }
                    }
                    return html;
                };

                return function (html) {
                    if (!html) {
                        return 0;
                    }
                    var width = 0;
                    var text = Ext.util.Format.stripTags(stripDisplayNone(html));
                    text = String(text).replace('&#160;', ' ');
                    text = String(text).replace('&nbsp;', ' ');
                    for (var i = 0; i < text.length; i++) {
                        var current = cellTextWidthMap[text.charCodeAt(i)];
                        // use char 'W' if char not found
                        current = Ext.isNumber(current) ? current : cellTextWidthMap[37];
                        width += current;
                    }
                    if (typeof(html) == 'string') {
                        html.replace(/<img/g, function (a) {
                            width += 16;
                            return '';
                        });
                        html.replace(/<div class="icon-tag/g, function (a) {
                            width += 12; // additional space needed for tag styling in policy
                            return '';
                        });
                        html.replace(/padding-right *: *[0-9]*/g, function (a) {
                            var parts = a.split(":");
                            if (parts.length > 1) {
                                var w = parseInt(parts[1]);
                                if (Ext.isNumber(w)) {
                                    width += w; // additional space needed for padding-right specification from PanOverrideIndicationRenderer
                                }
                            }
                            return '';
                        });
                    }
                    return width;
                };
            },
            /**
             * Test if object is empty
             */
            isEmpty: function (obj, allowBlank) {
                if (Ext.isObject(obj)) {
                    for (var prop in obj) {
                        if (obj.hasOwnProperty(prop)) {
                            return false;
                        }
                    }
                    return true;
                } else {
                    return Ext.isEmpty(obj, allowBlank);
                }
            },
            diff: function (o1, o2) {
                return Ext.encode(o1) !== Ext.encode(o2);
            },
            ordinalNumberSuffix: function (num) {
                var tenth = num % 10;
                var hundredth = num % 100;
                if (tenth == 1 && hundredth != 11) {
                    return num + "st";
                } else if (tenth == 2 && hundredth != 12) {
                    return num + "nd";
                } else if (tenth == 3 && hundredth != 13) {
                    return num + "rd";
                } else {
                    return num + "th";
                }
            }
        };
        
        return util;
    });
}, "Pan.base.util", require, exports, module);

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 * vim: sw=4 ts=4 expandtab
 */
