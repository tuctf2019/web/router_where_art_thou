# Router Where Art Thou -- TUCTF 2019

This challenge was as simple as recognizing that each of the three webpages was a fake router/firewall (`0.html` was a Fortinet router, `1.html` was a Sonicwall router, and `2.html` was a Palo Alto). After making that connection, all you had to do to get the flag was login with the default credentials of the website. For Fortinet it was admin/<null>, admin/password for Sonicwall and admin/admin for Palo Alto.
